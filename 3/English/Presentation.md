# Infrastructure as Code

## Introduction
Every computer system starts with an idea. The next step is prototyping, which means to play around with every aspect of the idea and produce a first version of your system based on your idea. After that you should test your system, maybe with some existing systems which needs to interact with yours. And finally – if all test passed - you release your new system to the customer.

After a while the customer would like to have some adjustments on the productive system. In some cases this means you first need to build a test environment and do all the test’s, you already did before realeasing the system to the customer, again after implementing the change.

Or the sales guy walks in and says “Hey I sold the same system we have for customer A to customer B. So you can just copy and paste from A to B, right?” And you then have to say: “Nope – that’s not how it’s gonna work, we need to start from scratch, sorry dude.”

I will call this the “traditional” approach from now on. This approach makes it complicated and very time consuming to reproduce every step needed to have a running system because you need to rebuild your whole infrastructure in case of an change or a rebuild of the system for a new customer.
That’s where Infrastructure as Code comes in place.

## Description
But first things first. Let’s start with what exactly do I mean when I talk about infrastructure as code (IaC).
With the infrastructure as code approach you actually have your whole infrastructure, or at least some parts of it, available as code in human readable files. Typically you want to track this file in a version control system like git or subversion – but to be honest, you do not want to use subversion ;-)

### Tools
These files who describes our infrastructure do not transform by magic to a real infrastructure, we need some tools which are doing the actual hard work, and deploy our infrastructure according to it.
There are tools like puppet, ansible, terraform, docker or chef who interpret our files and execute commands to reach the desired target state.

Most of these tools need a, at least, a “cloud-ready” infrastructure. This means they need to communicate to basic services like VM virtualization, DNS or basic networking over an API to get out the maximum of them. In IaC the goal is always to have as minimum human interaction as possible. The easiest way to have this APIs ready is to use a public cloud like Digitalocean, Amazones AWS, Microsofts Azure, Googles GCE or a public openstack cloud.

Or you could build your own private cloud with vmWare products or with a private openstack cloud.

### Testing
To ensure your infrastructure behaves always (everytime you deploy it for a new customer, or need to have a test environment) the same, you want to test your code and the behavior of the deployed infrastructure. This needs to be done to be 100% sure that everything is like it should be.

Some tools, like terraform, puppet or ansible have built-in mechanism who recheck the deployed stuff to ensure that everything works as it should be.
But in a larger scale environment this is some times not enough, and maybe you would like to include some security checks or other checks who prevent failures who already poped up in the past. Then you need a delivery pipeline which automatically tests your code and the deployed infrastructure in a test environment. If a check does not pass, an alert is generated and the faulty code will not be deployed to production. This is a very similar approach which is used specially in software development.

### Demo
Let’s say we would like to host a website with a database on Digitalocean, we would need to configure the following stuff:
- DNS names
	- www1.example.com which points to the web server
	- db01.example.com which points to the database server
- Web server
- Database server

For that we need two tools: terraform and puppet.
#### Terraform
~~~~
variable "do_token" {}
variable "ssh_fingerprint" {}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "www1.example.com" {
    image = "ubuntu"
    name = "www1.example.com"
    region = "fra1"
    size = "512mb"
    ssh_keys = [
      "${var.ssh_fingerprint}"
    ]
    provisioner "remote-exec" {
        scripts = [
          "puppet apply"
        ]
      }
}
resource "digitalocean_droplet" "db11.example.com" {
    image = "ubuntu"
    name = "db11.example.com"
    region = "fra1"
    size = "512mb"
    provisioner "remote-exec" {
        scripts = [
          "puppet apply"
        ]
    ssh_keys = [
      "${var.ssh_fingerprint}"
    ]
   }
}

resource "digitalocean_domain" "www1" {
   name = "www1.example.com"
   ip_address = "${digitalocean_droplet.www1.ipv4_address}"
}

resource "digitalocean_domain" "db01" {
   name = "db01.example.com"
   ip_address = "${digitalocean_droplet.db01.ipv4_address}"
}
~~~~
The above snipped creates all needed DNS entries and two machines in the digitalocean public cloud, both have 512MB memory and are provisioned with an ubuntu image as operating system. After the creation terraform opens a ssh connection to the newly created machines and applys the puppet code we define next.

#### Puppet
~~~~
node 'www1.example.com' {
  include common
  include nginx
}
node 'db1.example.com' {
  include common
  include mysql
}
~~~~
In the above example you can see that I do include the “common” module in both nodes. This is a great benefit when using infrastructure as code too. You can define functions, which as examples manages your ssh access keys or installs your favorite text editor, once and reuse it endless. If you want to install "ssh" on every node in the above example, you just need to define the following "common" class in your code:
~~~~
class common {
  package { 'ssh':
    ensure => installed
  }
}
~~~~
As puppet complies your code on the client (node) side to a task catalog which defines which steps need to be done to reach the state described in the code (declarative), vim would be reinstalled at every agent run if it should be uninstalled accidentally.

#### Deploy
Now we wrote all code that needs to be writen for our example. We can now use the teraform tool to deploy our infrastructure to the digitalocean public cloud:
~~~~
terraform plan
~~~~
gives us a plan, what terraform will do when we apply (deploy) our infrastructure.
~~~~
terraform apply
~~~~
deploys our infrastructure according to the code we defined above.

## Conclusion
To summarize the greatest benefits of using infrastructure as code are:
- reusability
- quality guarantee
- track all changes with a version control system
- with the help of the opensource community you do not need to reinvent the wheel all the time, just have a look on github or puppetforge to save you time
All this results in a less stressful work day for sysadmins and saves the company money, because you can easy boostrap a system from scretch for a new customer.

# Sources
- https://puppet.com
- https://en.wikipedia.org/wiki/Infrastructure_as_Code
- https://terraform.ios
- https://digitalocean.com

# Word list
| Word                        |  Explanation                             |
|-----------------------------|------------------------------------------|
| OpenStack                   | Big opensource project which develops a fully cloud with all aspects needed in the IT world |
| git                         | Most used version control system         |
| opensource                  | Opensource means that the underlying code of a software is public available and can be (in most cases) modified from everybody |
| terraform                   | Configuration management tool mostly used for managing cloud resources like machines, storage, databases or dns records |
| puppet                      | Popular configuration management tool mostly used on linux but windows compatible too |
| puppetforge                 | Opensource community for puppet, on http://puppetforge.com you can find many opensourced puppet modules |
| github                      | Biggest opensource community, uses git for all repositorys |
| digitalocean                | Digitalocean.com is a public cloud provider with an easily usable REST API |
| public- / private cloud     | A public or private cloud is a system which can be (in most cases) controlled over an API, the system consists of several servers used as compute or storage nodes. The network needs to be software defined.
| node                        | Server which is part of a system |
