# Präsentations stuff u so

## Auswertung
![Auswertung](pics/videoaufnahme.jpg)

## Lacher
- Lacher einbauen in die Präsentation
  - Versuchen zu Planen
  - Am besten innerhalb der ersten Minute
  - Zu Viel lachen == Komikveranstaltung

## Ablauf
- Ablauf (Inhaltsverzeichnis) Permanent sichtbar
  - auf PP Präsi
  - oder Flipboard
  - oder Auf Blatt

## Umsetzen
- auch beim Start nicht wegschauen (Begrüssung)
- 1 bis 2 Sekunden anschauen
- alle abholen
  - beim reinkommen: kurz stehenbleiben "cool" bleiben und Hallo zusammen sagen
- bei riesigem Publikum: in Sektoren "denken" - anschauen

## Stand
- Hüftbreit stehen
- Position ab und an wechseln
- zurückhaltend
  - dazwischen nicht reden (beim bewegen)
  - vor allem für Spannung aufbauen

## Rhetorik
- "ich erzähle euch" --> rhetorisch nicht gut (wir assoziieren schon ein wenig Langeweile mit auf)
  - besser: ich möchte euch mitnehmen auf die Reise....

## Einbezug Publikum (Fragen)
- Publikum fragen:
  - wer hat schon
  - wieso nicht
  - ...
  - Bedanken für die Frage/Input
- Eine form von Einbezug aktiviert Publikum
- Fragen klar abgrenzen wo die Fragen beantwortet werden --> Am Ende
  - Ausser wen es eine Verständnis Frage ist

## Sprache
- Wasser bereit halten, anstatt trockene Schnauze
- Der Anfang muss gelingen!

## Karten
- Karten zeigen das der Referent Vorbereitet ist
- Logo von dir und von Kunde auf der Rückseite
- A5
- Kartonpapier
- Durchnummerieren
- Darf schauen auf Karten --> macht auch pausen

## Gestik
 - Hände sollen nicht im Hosensack sein
 - In unserer Kultur wird die Hände zwischen Gurt und Schulter gerne gesehen (Empirische Daten) - Sympathisch


## Nervosität
 - Jeder weis selber was am besten ist zum Beruhigen
 - Wenn gut geübt, dann auch sicherer als ohne Übung
 - Karten (oder ähnliches) mit Notizen, wenn Blackout dass ich noch Infos holen kann
 - "Verbündete" im Publikum suchen
    - für "Energie" holen
 - Übung gegen Nervosität:
  - gegen die Wand pendeln für Bauch beruhigen
 - Eine gewisse Nervosität ist immer gut, weil keine ist nicht gut und Präsi failed
 - Musik verwenden um Motivation und Energie aufbauen.

## Wirkung Start
  - First impression (keine zweite chance für ersten Eindruck)
  - Publikum ist am Anfang am aufmerksamsten
    - Publikum entscheidend in den ersten 2,3 Minuten für- oder gegen mich
  - Nicht begrüssen, sondern zu erst einmal etwas sagen - Sprechpause - dann Begrüssung an 2. Stellen
    - aber nicht vergessen zu begrüssen!
  - Nicht unbedingt nötig herein zu laufen
  - Knalliger Einstieg
    - evt. Musik/Film
  -


### Einstieg gestallten
  - Video
  - Zitat
  - etwas visualisieren
  - Geschichte erzählen
  - Provokationen

## Wirkung Schluss
  - letzter Eindruck bleibt haften
  - Schluss muss nochmals was kommen !
  - Königsversion: Einstieg und Schluss verbinden
  - Nicht mit "danke für Ihre Aufmerksamkeit" stoppen
    - Merci sagen ist ok, aber nicht in den Floskeln

## Redezeit
  - Redezeiten Einhalten
  - Botschaften werden nicht ankommen, weil ich mein eigenes Zeit Management nicht im Griff habe

## Handskizzen
  - Am Flipboard sachen machen, Skizzen entstehen lassen
  -
## Power Point
- Name auf jedem Slide
- Power Point darf mich nicht Konkurrenzieren
- Agenda auf jedem Slide

## Kleine Geschenke
- nicht nach "erkaufen" aussehen
  - als schöne Geste

# Bühne
- überaschendes auf der rechten seite
- vertrautes und gewohntes kommt von links
- vor dem Tisch stehen - laufen

# Begrüssung
- vorher abklären wie man begrüssen sollte
  - komform
- normalerweise nicht spezielle begrüssung für einzelne Personen

# Kopf hoch
- Kopf nicht neigen
- Kopf gerade halten
- Kopf nach hinten
  - aber nicht hochnässig

# Floskeln
- nicht gebrauchen
- nimmt glaubwürdigkeit

# Laiser Pointer
- PP so machen das dies nicht nötig ist
- wenn dann nicht an einem Ort bleiben - weil dann gibts ein "geschlotter"

# Struktur
- Ablauf
  - immer sichtbar

# Innere Bilder
- selber aufbauen bevor Präsentation --> Vorbereitung
- (Mentale) - Vorbereitung ist wichtig
- Mental positiv vorbereiten
  - nicht sagen, ich versuche dann nicht umzufallen --> suggeriert das ich Probleme haben werde
- 
