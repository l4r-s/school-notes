# Test 2 

## Subnetz BITTE VERIFIZIEREN, DA STIMMT ETWAS NICHT

http://www.itslot.de/2013/12/ipv4-subnetting-berechnen-schritt-fur_22.html

| Netzanteil | Netzanteil | Netzanteil | Hostanteil |
| ----------- | ----------- | ----------- | ------------ |
| 131              | 102             | 4          | 1                |
| 255           | 255           |   248         | 224|
| [128][64][32][16][8][4][2][1] | [128][64][32][16][8][4][2][1]  | [128][64][32][16][8][][][] | [][][][][][][][] |

Es bleiben nun 3 Bit beim Netzanteil übrig, dadurch verschiebt sich unser Netz und der Netzanteil wird kleiner, welches uns mehr Spielraum für die Hosts gibt, 2^2 = 4.
Deshalb zählen wir nun in 4er Schritten hoch.

()2^11)-2 =2046

131.102.4.1 - 131.102.4.254

131.102.8.1 - 131.102.8.254

131.102.12.1 - 131.102.12.254

131.102.16.1 - 131.102.16.254



## Switch
- Ein Switch ist ein elektronisches Gerät zur Verbindung mehrerer Computer bzw. Netzwerksegmente in einem lokalen Netzwerk
- Ein Switch ist eine Weiche, ein Schalter, ein Verteiler
- Layer2 Switch: verarbeitet die 48 Bit MAC Adresse und legt dazu einen Eintrag in der SAT (Source Adress Table) an.
- Layer3 Switch: Verfügt über Managementfunktionen (Überwachung, Steuerung, usw.) resp. Verfügt über Routerfunktionen.
- Unterschied Switch zu HUB: 
 - HUB: Ein HUB kommuniziert per Broadcast ins komplette lokale Netzwerk.
 - Switch: Leitet Frames direkt an den Empfänger, da dieser bekannt ist.
- Verfahren:
 - Cut-Through: Leitet die Datenpakete direkt an die Zieladresse
 - Store and Forward (SF): Nimmt Datenpakete entgegen und speichert diese lokal auf dem Switch, die Pakete werden dort gefiltert und geprüft, danach werden diese an die Zieladresse weitergegeben.
- Vorteile der Verfahren:
 - Cut-Through: kleinere Latenzzeit, keine Bildung virtueller Netze möglich.
 - Store and Forward: Durch das Filtern der Pakete kann eine Fehlererkennung und Korrektur durchgeführt werden.
- Das Store and Forward Verfahren hat einen grösseren Anwendungsbereich.

## Repeater
- Repeater in der Netztechnik:
  - Repeater teilt das Netz in zwei physische Segmente, durch diesen Effekt erhöht der Repeater die Ausfallsicherheit des Netzes, da beim Ausfall eines Netzes das andere weiter bestehen kann.
  - 2 Typen von Repeatern:
    - Local-Repeater: Zwei lokale Netzsegmente miteinander verbinden.
    - Remote-Repeater: Zwei räumlich getrennte Repeater über ein Link-Segment verbinden. (Link Segment besteht aus zwei Repeatern die per Glasfaserkabel miteinander verbunden sind, dadurch können grössere Distanzen überwunden werden)
- Repeater in der Computertechnik:
  - Layer 1
  - Erweiterung der Netzsegmente
  - Repeater mit mehr als 2 Anschlüssen wird auch HUB oder Multi-Port-Repeater genannt.
- WLAN-Repeater:
  - Erhöhung der Reichweite eines drahtlosen Funknetzwerkes.
  - Datenübertragungsrate halbiert sich. Da der Repeater mit dem Accesspoint und den Clients kommuniziert.
  - Datenübertragungsrate wird nicht halbiert, wenn auf einer anderen Frequenz als zwischen dem Repeater und Router kommuniziert wird.
  - Dient dazu grössere Gebäude und Grundstücke mit einer ausreichenden Netzabdeckung zu versorgen.

## Gateway
- Protokoll Übersetzer
- Gateways sind eher rar geworden, da hauptsächlich über das IP Protokoll kommuniziert wird.
- Leitet alle Anfragen welche nicht zum Subnetz gehören an ein anderes Subnetz weiter.
- Erfüllt Funktionen eines Routers, Routing.
- Gateways können auf allen OSI Schichten eingesetzt werden. Wird aber Hauptsächlich im Transportlayer 3 verwendet.
- Kann mit Routingprotokollen umgehen.

## DHCP
- Dynamic Host Configuration Protocol
- Arbeitet ausschliesslich mit UDP
- Zuweisung der Netzwerkkonfiguration an Clients
- Keine manuelle Konfiguration der Netzwerkschnittstelle notwendig, wenn ein Client an ein lokales Netz angeschlossen wird. 
- DNS SubnetMaske und IP werden automatisch an Clients vergeben.
- Layer 3 Transportlayer
.