---
title: "STE Portfolio"
author: [Lars Bättig (me@l4rs.net)]
date: 2017-06-03
subject: "STE Portfolio"
tags: [IoT, Raspberry]
subtitle: "TSBE 16B"
titlepage: true
titlepage-rule-height: 4
...


---
title: "Vinaque sanguine metuenti cuiquam Alcyone fixus"
author: [Author Name]
date: 2017-02-20
subject: "Markdown"
tags: [Markdown, Example]
subtitle: "Aesculeae domus vincemur et Veneris adsuetus lapsum"
titlepage: true
titlepage-color: 06386e
titlepage-text-color: ffffff
titlepage-rule-color: ffffff
titlepage-rule-height: 1
...

# Internet of Things (IoT)

>The Internet of things (stylised Internet of Things or IoT) is the internetworking of physical devices, vehicles (also referred to as "connected devices" and "smart devices"), buildings, and other items—embedded with electronics, software, sensors, actuators, and network connectivity that enable these objects to collect and exchange data

[Wikipedia](https://en.wikipedia.org/wiki/Internet_of_things)

Das Internet der dinge beschreibt einen neuen Trend der "alles" miteinander vernetzt.
Vorallem Sensoren und Aktoren werden vernetzt, häufig mit IP jedoch oftmals auch mit anderen Protokollen die Energieschonender als TCP/IP arbeiten:
- Lora (LoraWAN)
- Bluetooth low Power
- Zigbee

## Vor- und Nachteile von IoT
>Peripheriegeräte im Netzwerkbetrieb einsetzen, was verstehen Sie darunter?

Geräte die Sensoren und Aktoren verbaut haben werden "Kommunikationsfähig" gemacht in dem diese über z.B. Ethernet kommunizieren können - häufig zu einer Cloud. Nebst Ethernet bestehen noch weitere Protokolle die meist einen Gateway zum Internet (TCP/IP, Ethernet) einsetzen, wie z.B. Lora.

> Was könnte Ihrer Meinung nach unter „Things“ alles verbunden werden? Zählen Sie auf!

Alles?! Grundsätzlich sind aktoren und sensoren in heutigen Elektronischen Geräten verbaut. Die informationen von Sensoren können verarbeitet werden und auf Basis von diesen Daten können die Aktoren gesteuert werden.

>Haben Sie weiter futuristische Beispiele? (Die Frage bezieht sich auf futuristische Iot Geräte)

Grundsätzlich ist alles denkbar. In der heutigen Zeit gibt es jedoch Technische barrieren, wie z.B. Srromverbrauch, grösse der Energiequelle und oder des Mikrocontrollers.
Speziell im Health Care bereich haben IoT Konzepte ein sehr grosses Potenziall:
- Herzschrittmacher
- Insulien Pumpen
- Intensiev Station: Überwachung des Patienten
- ...
Jedoch muss besonders im Health Care bereich ein spezielles augenmerk auf die Sicherheit gelegt werden.

> Welche Vorteile können Sie sich vorstellen? (Die uns Iot im Alltag bringt)

Durch IoT können wir unser Leben angenehmer gestallten und unsere Arbeiten Maschienen abgeben.

> Können Sie diesen Vergleich nachvollziehen? Wo sehen Sie selber die Gefahren? (In Bezug auf Iot)

Im Bereich IoT wird es gefährlich sobald Akteure im Spiel sind. Wird z.B. in einem Serverraum mittels Sensoren die Temperatur ermittelt und diese Unverschlüsselt zu einem Server versendet werden, welcher die Klimaanlage des Serverraumes steuert, könnte ein Angreifer falsche Temperatur Daten liefern und so erheblichen Schaden anrichten.

# xRDP

~~~
sudo apt-get remove xrdp vnc4server tightvncserver
sudo apt-get install tightvncserver
sudo apt-get install xrdp
~~~

Da Raspian Pixel vnc4server vorinstalliert hat und dieser nicht kompatibel mit xrdp ist, muss dieser zuerst deinstalliert werden.  [Quelle](http://raspberrypi.stackexchange.com/questions/56413/error-problem-connecting-to-raspberry-pi-3-with-xrdp)

# Raspberry Security
## Update
~~~
apt-get update && apt-get -y upgrade
~~~

## Passwort ändern

Raspian hat einen default User "pi" mit dem default Passwort "pi", daher muss zumindest das Passwort geändert werden.

~~~
passwd [username]
~~~

## Neuen Benutzer erstellen
~~~
adduser [username]
~~~

### Sudo rechte
~~~
usermod -aG sudo [username]
~~~

# RPi Monitor

## Installation
~~~
sudo wget http://goo.gl/vewCLL -O /etc/apt/sources.list.d/rpimonitor.list
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 2C0D3C0F
sudo apt-get update
sudo apt-get install rpimonitor
~~~

## Update
~~~
sudo apt-get update
sudo apt-get upgrade # aktualisiert alle packete
sudo /etc/init.d/rpimonitor update
~~~

## Starten
~~~
systemctl start rpimonitord
~~~

## Stoppen
~~~
systemctl stop rpimonitord
~~~

## Aktivieren (autostart)
~~~
systemctl enable rpimonitord
~~~

## DEaktivieren (autostart)
~~~
systemctl disable rpimonitord
~~~

## Zugriff
~~~
http://raspi.l4rs.net:8888
~~~

# Webserver
## Installation
~~~
apt-get update
apt-get install apache2
~~~

## Starten
~~~
systemctl start apache2
~~~

## Stoppen
~~~
systemctl stop apache2
~~~

## Aktivieren (autostart)
~~~
systemctl enable apache2
~~~

## Deaktivieren (autostart deaktivieren)
~~~
systemctl disable apache2
~~~

# Nextcloud

## Installation
~~~
apt-get install unzip php
wget https://download.nextcloud.com/server/releases/nextcloud-12.0.0.zip
cd /var/www/html
unzip nextcloud-12.0.0.zip
~~~

## Zugriff
~~~
http://raspi.l4rs.net/nextcloud
~~~

# Printserver

## Installation
~~~
apt-get install cups
~~~

## Starten
~~~
systemctl start cups
~~~

## Stoppen
~~~
systemctl stop cups
~~~

## Aktivieren (autostart)
~~~
systemctl enable cups
~~~

## Deaktivieren (autostart)
~~~
systemctl disable cups
~~~

## Zugriff
~~~
http://raspi.l4rs.net:631
Username: lars
Password: *****
~~~

# Webmin
## Installation
~~~
apt-get install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python
wget http://prdownloads.sourceforge.net/webadmin/webmin_1.840_all.deb
dpkg --install webmin_1.840_all.deb
~~~

## Starten
~~~
systemctl start webmin
~~~

## Stoppen
~~~
systemctl stop webmin
~~~

## Aktivieren (autostart)
~~~
systemctl enable webmin
~~~

## Deaktivieren (autostart)
~~~
systemctl disable webmin
~~~

## Zugriff
~~~
http://raspi.l4rs.net:10000
Username: lars
Password: ******
~~~

# LED GPIO

## Installation
~~~
apt-get install wiringpi
~~~

## wiringpi mapping
~~~
gpio mode 0 out # set pin 0 to output mode
gpio write 0 1 # write a logical 1 to the pin 0
gpio write 0 0 # write a logical 0 to the pin0
~~~

## gpio mapping
~~~
gpio -g mode 17 out
gpio -g write 17 1 # use the gpio pins (17 is equal to 0 in wiringpi)
gpio -g write 17 0
~~~

## Wiring:
- Ground (pin6) == ground on breadboard
- GPIO17 (pin11)== positiv on breadboard

# 1-Wire
~~~~
sudo modprobe w1-gpio
sudo modprobe w1-therm
~~~~

## Wiring
- Ground (pin 6) == ground breadboard
- 3.3V (Pin1) == positiv breadboard
- GPIO4 (Pin7) == yellow cable

## Pins
- 10 == 3.3V
-  9 == data
-  8 == ground

# OpenHAB LED GPIO

1. install openhab image
2. run as root /usr/lib/systemd/system/openhab.service
3. systemctl daemon-reload
4. systemctl restart openhab2
5. create file /etc/openhab2/services/gpio.cfg:

~~~~
sysfs=/sys
debounce=10
~~~~

7. create file /etc/openhab2/items/led.items:


## SSH Login:
~~~
Username: openhabian
Password: openhabian
~~~

## Web

~~~
http://192.168.1.12:8080/basicui/app?sitemap=home
~~~

# Raspberry Pi Pin Layout

![Versionen](pi3_gpio.png)\

(Nic unten)

# Temperatur Luftfeuchtigkeit Messung PoC

Im Staging und Lager Raum in welchem Netzwerkgeräte installiert sowie gelagert werden muss die Temperatur und Luftfeuchtigkeit überwacht werden.

## Zugriff Dashboard

[https://iot.l4rs.net](https://iot.l4rs.net/ui)

## Komponenten

### nodered.l4rs.net
- VM auf Proxmox
- Centos 7
- Nodered Applikations Server

### rproxy.l4rs.net
- VM auf Proxmox
- Ubuntu 16.04
- Nginx Reverse Proxy

### ESP8266
  - ASM302 Sensor
  - Arduino Code

## Datenflow
1. ESP8266 liest alle 3 Sekunden die Aktuellen werte (Temperatur & Luftfeuchtigkeit)
2. Diese Werte werden via HTTP GET durch den Reverse Proxy zum NodeRed Flow ubertragen
3. Im Flow werden die Werte (Device ID, Temperatur und Luftfeuchtigkeit) aufgeschlüsselt in Variablen und dem Dashboard Node zum Anzeigen zur Verfügung gestellt.
### Flow
![Bild Flow](nodered-flow.png)\

## Installation Node-Red
~~~~
yum -y install nodejs
npm install -g --unsafe-perm node-red
~~~~

## Installation node-red Dashboard node
NodeRed bietet ein Dashboard, welches es ermöglicht die Daten grafisch darzustellen.

~~~~
cd ~/.node-red
npm i node-red-dashboard
~~~~

## Node-Red ausführen
Zum entwicklen von neuen Flows ist es am einfachsten Node-Red in der CLI laufen lassen.
Hierzu eignet sich ein Terminal Multiplexer wie tmux.

~~~~
apt-get install tmux
tmux
node-red
~~~~

Mittels Ctrl-B + D kann nun die Session "detached" werden und die SSH Session geschlossen werden. Nod-Red wird auf dem Server immer noch weiterlaufen.

Nach dem Aufbau einer neuen SSH Session kann mittels "tmux a" die laufende tmux Session wieder angezeigt werden.


## Arduino Source Code (ESP8266)

~~~~
#include "ESP8266WiFi.h";
#include <DHT.h>;


// WiFi parameters
const char* ssid = "SSID_WLAN";
const char* password = "SSID_PASSWORD";

const char* host = "iot.l4rs.net";
const int httpPort = 80;

#define DHTPIN 5 // == PIN D1 on NodeMCU EP8266
#define DHTTYPE DHT22   // DHT 22  (AM2302)


 //// Initialize DHT sensor
DHT dht(DHTPIN, DHTTYPE);

float hum;
float temp;

void setup()
{
    Serial.begin(115200);
    dht.begin();

    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void loop()
{

      Serial.print("Connecting to [host] / [port] ");
      Serial.println(host);
      Serial.println(httpPort);

      WiFiClient client;
      if (!client.connect(host, httpPort)) {
        Serial.println("connection failed");
        return;
     }

    hum = dht.readHumidity();
    temp= dht.readTemperature();

    Serial.print("temp: ");
    Serial.println(temp);

    Serial.print("hum: ");
    Serial.println(hum);

    Serial.println("sending data");     
    client.print(String("GET /data-home/?devid=esp8266&temp=" + String(temp) + "&hum=" + String(hum) + " HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n"));
    Serial.println("done sending... waiting for answer");
    delay(10);

    while(client.available()){
      String line = client.readStringUntil('\r');
      Serial.print(line);
    }

    Serial.println();
    Serial.println("closing connection");

    // 1000 milliseconds == 1 second
    delay(3000); //15min == 900000
}
~~~~

## Installation Nginx Reverse Proxy
~~~~
apt-get update
apt-get -y install nginx
~~~~

### Konfiguration
~~~~
server {
  listen 80;
  server_name iot.l4rs.net;
  root         /usr/share/nginx/iot.l4rs.net;

  access_log /var/log/nginx/iot.l4rs.net/access.log;
  error_log /var/log/nginx/iot.l4rs.net/error.log;

  location ~ /.well-known {
     allow all;
  }


  rewrite ^/ui(.*)$ https://iot.l4rs.net/ui redirect;

  location /data/ {
    proxy_pass http://192.168.15.37:1880/data/;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
  }

  location /data-home/ {
    proxy_pass http://192.168.15.37:1880/data-home/;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
  }

}

server {
    listen 443 ssl http2;
    server_name iot.l4rs.net
    root   /usr/share/nginx/iot.l4rs.net;

    access_log /var/log/nginx/iot.l4rs.net/https-access.log;
    error_log /var/log/nginx/iot.l4rs.net/https-error.log;

    ssl_certificate /etc/letsencrypt/live/iot.l4rs.net/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/iot.l4rs.net/privkey.pem;

    include snippets/ssl-params.conf;


    location /ui/ {
      proxy_pass http://192.168.15.37:1880/ui/;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
    }
}
~~~~

### Zertifikat mit Let's Encrypt ausstellen
~~~~
apt-get install certbot
certbot certonly --webroot-path /usr/share/nginx/iot.l4rs.net/ -d iot.l4rs.net
~~~~

# LED Steuerung mit Raspberry Pi, Golang Webserver und OpenHAB


## LED Webserver auf Raspberry PI
Untenstehender Code wird nach Kompilieren und ausführen des Binarys einen Webserver auf Port 12345 zur Verfügung stellen.

### Golang installation auf dem Raspberry
~~~~
apt-get -y install golang
export GOPATH=$HOME/go
~~~~
### Source Code

~~~~
package main

import (
    "log"
    "net/http"
    "fmt"
    "github.com/stianeikeland/go-rpio"
    "github.com/gorilla/mux"
    "os"
)

var (
	// PIN19 GPIO10
	pin = rpio.Pin(10)
)

func LedOn(w http.ResponseWriter, req *http.Request) {
  if err := rpio.Open(); err != nil {
      fmt.Println(err)
      os.Exit(1)
  }

  if pin.Read() == 1 {
    w.WriteHeader(http.StatusOK)
	  fmt.Fprintf(w, "LED already enabled \n")
    fmt.Println("LED already enabled")
  } else {
    pin.Output()
    pin.Toggle()
    w.WriteHeader(http.StatusOK)
    fmt.Fprintf(w, "LED enabled \n")
    fmt.Println("LED enabled")
  }
}

func LedOff(w http.ResponseWriter, req *http.Request) {
  if err := rpio.Open(); err != nil {
      fmt.Println(err)
      os.Exit(1)
  }

  if pin.Read() == 0 {
    w.WriteHeader(http.StatusOK)
	  fmt.Fprintf(w, "LED already disabled \n")
    fmt.Println("LED already disabled")
  } else {
    pin.Output()
    pin.Toggle()
    w.WriteHeader(http.StatusOK)
    fmt.Fprintf(w, "LED disabled \n")
    fmt.Println("LED disabled")
  }
}

func main() {
    router := mux.NewRouter()
    router.HandleFunc("/on", LedOn).Methods("GET")
    router.HandleFunc("/off", LedOff).Methods("GET")

    log.Fatal(http.ListenAndServe(":12345", router))
}
~~~~

### Kompilieren

~~~~
go get
go build
~~~~

### Ausführen

~~~
./led-webserver
~~~

### LED Einschalten
~~~~
curl http://localhost:12345/on
~~~~

### LED Ausschalten
~~~~
curl http://localhost:12345/off
~~~~

## OpenHAB Konfiguration

### HTTP Binding
HTTP Binding Addon muss auf dem OpenHAB System installiert sein.

### Sitemap
$OPENHABCONFIG/sitemaps/led.sitemap:

~~~~
sitemap led label="Raspi LED" {
        Switch item=raspiled
}
~~~~

### Item
$OPENHABCONFIG/items/raspiled.items:

~~~~
Switch raspiled {
        http=">[ON:GET:http://192.168.55.23:12345/on] >[OFF:GET:http://192.168.55.23:12345/off]"
}
~~~~

# Portfolio
Das Portfolio wurde in Markdown geschrieben und mit Git versioniert.
Das Repository ist öffentlich erreichbar auf [Github](https://github.com/l4r-s/school-notes/blob/master/3/Raspberry/Portfolio/Portfolio.md).

Mittels pandoc läst sich ein PDF generieren.

## Requirements
- [Pandoc](http://pandoc.org/demos.html)
- [Template](https://github.com/Wandmalfarbe/pandoc-latex-template)

## Generieren

~~~~
pandoc Portfolio.md -o STE_Portfolio_Lars_Bättig.pdf --from markdown --template eisvogel --listings -V lang=de -N --toc --variable urlcolor=cyan
~~~~
