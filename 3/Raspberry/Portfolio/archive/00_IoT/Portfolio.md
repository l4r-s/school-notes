# Internet of Things (IoT)
*AB126-00_Kann_Liste_IoT*

>The Internet of things (stylised Internet of Things or IoT) is the internetworking of physical devices, vehicles (also referred to as "connected devices" and "smart devices"), buildings, and other items—embedded with electronics, software, sensors, actuators, and network connectivity that enable these objects to collect and exchange data

[Wikipedia](https://en.wikipedia.org/wiki/Internet_of_things)

Das Internet der dinge beschreibt einen neuen Trend der "alles" miteinander vernetzt.
Vorallem Sensoren und Aktoren werden vernetzt, häufig mit IP jedoch oftmals auch mit anderen Protokollen die Energieschonender als TCP/IP arbeiten:
- Lora (LoraWAN)
- Bluetooth low Power
- Zigbee

## Vor- und Nachteile von IoT
>Peripheriegeräte im Netzwerkbetrieb einsetzen, was verstehen Sie darunter?

Geräte die Sensoren und Aktoren verbaut haben werden "Kommunikationsfähig" gemacht in dem diese über z.B. Ethernet kommunizieren können - häufig zu einer Cloud. Nebst Ethernet bestehen noch weitere Protokolle die meist einen Gateway zum Internet (TCP/IP, Ethernet) einsetzen, wie z.B. Lora.

> Was könnte Ihrer Meinung nach unter „Things“ alles verbunden werden? Zählen Sie auf!

Alles?! Grundsätzlich sind aktoren und sensoren in heutigen Elektronischen Geräten verbaut. Die informationen von Sensoren können verarbeitet werden und auf Basis von diesen Daten können die Aktoren gesteuert werden.

>Haben Sie weiter futuristische Beispiele? (Die Frage bezieht sich auf futuristische Iot Geräte)

Grundsätzlich ist alles denkbar. In der heutigen Zeit gibt es jedoch Technische barrieren, wie z.B. Srromverbrauch, grösse der Energiequelle und oder des Mikrocontrollers.
Speziell im Health Care bereich haben IoT Konzepte ein sehr grosses Potenziall:
- Herzschrittmacher
- Insulien Pumpen
- Intensiev Station: Überwachung des Patienten
- ...
Jedoch muss besonders im Health Care bereich ein spezielles augenmerk auf die Sicherheit gelegt werden.

> Welche Vorteile können Sie sich vorstellen? (Die uns Iot im Alltag bringt)

Durch IoT können wir unser Leben angenehmer gestallten und unsere Arbeiten Maschienen abgeben.

> Können Sie diesen Vergleich nachvollziehen? Wo sehen Sie selber die Gefahren? (In Bezug auf Iot)
