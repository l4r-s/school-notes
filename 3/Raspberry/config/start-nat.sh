#!/bin/bash

EXT="wlp3s0"
INT="enp0s25"

sudo sysctl net.ipv4.ip_forward=1
sudo ip a add 192.168.1.1/24 dev enp0s25
sudo firewall-cmd --direct --add-rule ipv4 nat POSTROUTING 0 -o $EXT -j MASQUERADE
sudo firewall-cmd --direct --add-rule ipv4 filter FORWARD 0 -i $INT -o $EXT -j ACCEPT
sudo firewall-cmd --direct --add-rule ipv4 filter FORWARD 0 -i $EXT -o $INT -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo systemctl start dhcpd4.service
