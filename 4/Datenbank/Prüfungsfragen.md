# Pruefungsfragen

## Termine:
- 22.09.2017
- 24.11.2017
- 10.01.2018


## 22.09.2017

1. Welches sind die Umgebungsvariablen?
    - ${ORACLE_BASE}
    Basis Verzeichnis von Oracle
    - ${ORACLE_HOME}
    Spezifisches Verzeichnis für jede Datenbankversion, ist wichtig bei mehreren Oracle Versionen auf einem Server.
    - ${ORACLE_SID}
    Instanz- Datenbankname auf einem System
    - ${TNS_ADMIN}
    Ort wo die Netzwerkkonfiguration von Oracle abgelegt ist.
2. Wie formatiere ich die Ausgabe in der Konsole? (sqlplus)
    - set lines [n] | Setzt die Zeilenlänge auf n Zeichen
    - set pages [n] | Setzt	 die Seitenlänge auf n Zeilen
    - col [Name] format a[n] | Beschränkt Character Felder auf n Character
    - col [Name] format 999 | Beschränkt numerische	Felder auf z.B. 3 Stellen. 5 Stellen = 99999
3. Was ist ein ERM
    - ERM ist ein Datenmodell (Entity Relationship Model)
4. Was beinhaltet ein Datenmodell, ERM?
    - Ein ERM beinhaltet ein ERD (grafische Darstellung) sowie die darin enthaltenen Elemente und deren Bedeutung und Beziehung zueinander.
5. Was bedeuten diese Begriffe
    - Entity, Entität, Entitätsmenge, Tupel, Row, Datensatz, Beziehung, Relation
        - Entität = eine Zeile in der Datenbank die identifizierbar ist. Entitäten stehe in Beziehung zu anderen Entitäten. Eine Entität besteht aus mehreren Attributen (Name, Vorname, Strase. usw)
        - Entitätsmenge = Mehrere Zeilen mit gleichen Identifikatoren in der Datenbank
        - Relation = Sammlung von Tabellen
        - Tupel = Mehrere Attribute aus einer Tabelle
        - Row = Eine Reihe in der Tabelle
        - Datensatz = 

6. Was ist eine Kardinalität?
    - Die Kardinalität bestimmt die Beziehungen zwischen zwei Entitäten.
    - Folgende Beziehungen sind möglich: 1:1, 1:0/1, 1:n, 1:0/n
        - Eine Person wohn an einem Ort = 1:1
        - An einem Ort wohnen mehrere Personen = 1:n
    - Kardinalität wird in Zeichnungen abgebildet. Siehe Folie  Datenmodellierung Seite 14. 
  
----
0. A4 Spicker.
1. 01 - Einführung
2. 03 - Must Know
3. Environment Variablen setzen
4. SQLPLUS starten (connecten)
5. Formatierungsbefehle sql
6. Datenmodellierung (ohne SQL Syntax)
