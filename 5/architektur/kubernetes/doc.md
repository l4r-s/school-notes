---
title: "K8s documentation"
author: [Lars Bättig (me@l4rs.net)]
date: 2018-02-24
subject: "TSBE - Architektur"
tags: [Ceph, Architektur, kubernetes]
subtitle: "TSBE - Architektur"
titlepage: true
lang: en
toc: true
titlepage-rule-height: 4
...


# Lab setup

| Username | Password | Port (for public access) |
|----------|----------|--------------------------|
| student2 | ssh-key  | 30002                    |

Jumphost IP: 35.205.224.103

# Gcloud VM build

# Manually

Used for managing VM's on the google cloud.

~~~bash
# stop vm:
$ gcloud compute instances stop --zone europe-west1-d s2-node1

gcloud compute instances stop --zone europe-west1-d s2-node1
gcloud compute instances stop --zone europe-west1-d s2-node2
gcloud compute instances stop --zone europe-west1-d s2-node3

# start vm:
$ gcloud compute instances start --zone europe-west1-d s2-node1
# delete vm:
$ gcloud compute instances delete --zone europe-west1-d s2-node1
~~~

## Script

Or we can do it with a somehow crappy script:

~~~bash
#!/bin/bash

for i in `seq 1 3`
do
        gcloud compute instances $1 --zone europe-west1-d s2-node$i &
        sleep 5
done
~~~

~~~bash
# start all vms
./vms.sh start

# stop all vms
./vms.sh stop
~~~

# Kubernetes and ceph setup

## VMs

First we need to spin up 3 nodes, which will run kubernetes and ceph as storage backend.

~~~bash
$ gcloud compute instances create s2-node1 --image-family ubuntu-1604-lts --image-project ubuntu-os-cloud --machine-type n1-standard-2 --zone europe-west1-d

$ gcloud compute instances create s2-node2 --image-family ubuntu-1604-lts --image-project ubuntu-os-cloud --machine-type n1-standard-2 --zone europe-west1-d

$ gcloud compute instances create s2-node3 --image-family ubuntu-1604-lts --image-project ubuntu-os-cloud --machine-type n1-standard-2 --zone europe-west1-d
~~~

## SSH access and software update

With `gcloud compute ssh <hostname>` we can directly ssh to our new nodes. For the ceph part we need to ssh from one node to the other(s) without gcloud and without password. Therefore we need to setup a ssh key pair and deploy the public part on all nodes. Furthermore we need to bring our newly created vms to last patch state.

> `gcloud compute ssh` will generate a seperate ssh private key. The public part will be copied over the API to the machine.

Generate new SSH key pair on the jumphost:

~~~bash
ssh-keygen # generates our ssh key pair used for ceph on the jumphost
~~~

The following steps need to be done on s2-node{1-3}:

~~~bash
gcloud compute ssh s2-node{1-3} # connects to the node 's2-node1'

sudo -i
apt-get update && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y
useradd -d /home/cephd -m cephd -s /bin/bash
echo "cephd ALL = (root) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/cephd
chmod 0440 /etc/sudoers.d/cephd
mkdir -p /home/cephd/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8yb2VRps7Avui32+DdrE5JEmo/4ZlmOialIJghGTtHe8HCib6HRPwCEZ0DrFYuWa2tfH2odrcbTkw8SAgt30dKSVCZr3DE8QfKz1VvY1i7KqCxU5lVfGqUDL8fHFTgFFwqexf91EWemGx25+i7L6j+6oxfVWhqJwgLjbnDnd3dCSMaWUcKR+Z1GN1Q7ZpUOfwcO9pHcsTfqdOPhxw6CtSBDI0mDwxBHof4At6IHDkR4PJbW6utmkTa5GHebNKf6MZa6iUg/JPZYyumguD6Z80on4YiXM8SBnXguiDfGqAr/M8Wh96q1wt/p2n2U+1tGF0acLkolbfBfxJh2g8STlz student2@jumphost" > /home/cephd/.ssh/authorized_keys
chown -R cephd:cephd /home/cephd
chmod 0600 /home/cephd/.ssh/authorized_keys
~~~

On the jumphost we define now the following content in our ssh configuration `.ssh/config`:

~~~bash
Host s2-node1 s2-node2 s2-node3
        User cephd
        IdentityFile ~/.ssh/ceph
~~~

This tells the ssh client to always use the user `cephd` and the ssh key `.ssh/ceph` for accessing the 3 nodes s2-node{1-3}.

## ceph installation

On the jumphost we create a working directory for our ceph deployment:

~~~bash
mkdir ceph
cd ceph
~~~

No we can use the `ceph-deploy` tool to install and deploy ceph to our 3 nodes `s2-node{1-3}`:

~~~bash
ceph-deploy install s2-node1 s2-node2 s2-node3
ceph-deploy new s2-node1 s2-node2 s2-node3
~~~

Now we need to extend the ceph configuration `ceph.conf`. The following two lines need to be added:

From the ceph documentation: http://docs.ceph.com/docs/jewel/rados/configuration/filesystem-recommendations/
The two lines adjust the ceph object length, which needs to be ext4 compatible.

~~~bash
osd max object name len = 256
osd max object namespace len = 64
~~~

Let's deploy our ceph cluster:

~~~bash
ceph-deploy mon create-initial
~~~

Now we need to create a osd directory on every node:

~~~bash
ssh s2-node1 sudo mkdir /var/local/osd0
ssh s2-node2 sudo mkdir /var/local/osd1
ssh s2-node3 sudo mkdir /var/local/osd2
~~~

Prepare the osd for use with the cluster:

~~~bash
ceph-deploy osd prepare s2-node1:/var/local/osd0 s2-node2:/var/local/osd1 s2-node3:/var/local/osd2
~~~

This is an ugly fix for a permission issue that we have with the osd disks. Never do this in production :):

~~~bash
ssh s2-node1 sudo chmod -R 777 /var/local/osd0
ssh s2-node2 sudo chmod -R 777 /var/local/osd1
ssh s2-node3 sudo chmod -R 777 /var/local/osd2
~~~

Activate the osd disks

~~~bash
ceph-deploy osd activate s2-node1:/var/local/osd0 s2-node2:/var/local/osd1 s2-node3:/var/local/osd2
~~~

Deploy the cluster keys to the nodes so we can use them to work with the cluster.

~~~bash
ceph-deploy admin s2-node1 s2-node2 s2-node3
~~~


Ensure that ceph osds get automatically started on system boot.

~~~bash
ssh s2-node3 sudo systemctl enable ceph-osd@2
ssh s2-node2 sudo systemctl enable ceph-osd@1
ssh s2-node1 sudo systemctl enable ceph-osd@0
~~~

Test the ceph installation:

~~~bash
student2@jumphost:~/ceph$ ssh s2-node1 sudo ceph -w
    cluster 04f5f260-8ff1-46ba-be68-8ecac5e7c22c
     health HEALTH_OK
     monmap e1: 3 mons at {s2-node1=10.132.0.3:6789/0,s2-node2=10.132.0.4:6789/0,s2-node3=10.132.0.5:6789/0}
            election epoch 6, quorum 0,1,2 s2-node1,s2-node2,s2-node3
     osdmap e15: 3 osds: 3 up, 3 in
            flags sortbitwise,require_jewel_osds
      pgmap v78: 64 pgs, 1 pools, 0 bytes data, 0 objects
            19899 MB used, 9637 MB / 29585 MB avail
                  64 active+clean

2018-02-03 10:54:19.865221 mon.0 [INF] pgmap v78: 64 pgs: 64 active+clean; 0 bytes data, 19899 MB used, 9637 MB / 29585 MB avail
2018-02-03 10:54:21.897371 mon.0 [INF] pgmap v79: 64 pgs: 64 active+clean; 0 bytes data, 19899 MB used, 9637 MB / 29585 MB avail
2018-02-03 10:54:23.950823 mon.0 [INF] pgmap v80: 64 pgs: 64 active+clean; 0 bytes data, 19899 MB used, 9637 MB / 29585 MB avail
~~~

## Kubernetes installation

Create working directory:

~~~bash
mkdir k8s && cd k8s
~~~

Install kubernetes and dependencies (needs to be done on all 3 hosts s2-node{1-3}):

~~~bash
sudo -i
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF > /etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y docker.io
apt-get install -y kubelet kubeadm kubectl kubernetes-cni
~~~

### Kubernetes initialization

~~~bash
root@s2-node1:~# kubeadm init --pod-network-cidr 10.244.0.0/16
[init] Using Kubernetes version: v1.9.2
[init] Using Authorization modes: [Node RBAC]
[preflight] Running pre-flight checks.
        [WARNING FileExisting-crictl]: crictl not found in system path
[certificates] Generated ca certificate and key.
[certificates] Generated apiserver certificate and key.
[certificates] apiserver serving cert is signed for DNS names [s2-node1 kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local] and IPs [10.96.0.1 10.132.0.3]
[certificates] Generated apiserver-kubelet-client certificate and key.
[certificates] Generated sa key and public key.
[certificates] Generated front-proxy-ca certificate and key.
[certificates] Generated front-proxy-client certificate and key.
[certificates] Valid certificates and keys now exist in "/etc/kubernetes/pki"
[kubeconfig] Wrote KubeConfig file to disk: "admin.conf"
[kubeconfig] Wrote KubeConfig file to disk: "kubelet.conf"
[kubeconfig] Wrote KubeConfig file to disk: "controller-manager.conf"
[kubeconfig] Wrote KubeConfig file to disk: "scheduler.conf"
[controlplane] Wrote Static Pod manifest for component kube-apiserver to "/etc/kubernetes/manifests/kube-apiserver.yaml"
[controlplane] Wrote Static Pod manifest for component kube-controller-manager to "/etc/kubernetes/manifests/kube-controller-manager.yaml"
[controlplane] Wrote Static Pod manifest for component kube-scheduler to "/etc/kubernetes/manifests/kube-scheduler.yaml"
[etcd] Wrote Static Pod manifest for a local etcd instance to "/etc/kubernetes/manifests/etcd.yaml"
[init] Waiting for the kubelet to boot up the control plane as Static Pods from directory "/etc/kubernetes/manifests".
[init] This might take a minute or longer if the control plane images have to be pulled.
[apiclient] All control plane components are healthy after 42.001858 seconds
[uploadconfig] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[markmaster] Will mark node s2-node1 as master by adding a label and a taint
[markmaster] Master s2-node1 tainted and labelled with key/value: node-role.kubernetes.io/master=""
[bootstraptoken] Using token: 708e2c.105019419fd2ae81
[bootstraptoken] Configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstraptoken] Configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstraptoken] Configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstraptoken] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
[addons] Applied essential addon: kube-dns
[addons] Applied essential addon: kube-proxy

Your Kubernetes master has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of machines by running the following on each node
as root:

kubeadm join --token 832072.05a282299ba4af5e 10.132.0.3:6443 --discovery-token-ca-cert-hash sha256:eb6f26d0bd99c164d01fecfdbd99f04d9263e9e94cc1f58844e42893d0a24411
~~~

As the cephd user we need to adjust the kube configuration for using our newly created cluster:

~~~bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
~~~

Now we tell the master node that he is allowed to spin up pods too:

~~~bash
$ cephd@s2-node1:~$ kubectl taint node --all node-role.kubernetes.io/master:NoSchedule-
$ node "s2-node1" untainted
~~~

Flannel network configuration:

~~~bash
cephd@s2-node1:~$ wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
cephd@s2-node1:~$ kubectl apply -f kube-flannel.yml --namespace=kube-system
serviceaccount "flannel" created
configmap "kube-flannel-cfg" created
daemonset "kube-flannel-ds" created
~~~

Check that all needed pods are up and running:

~~~bash
cephd@s2-node1:~$  kubectl get pods --all-namespaces
NAMESPACE     NAME                               READY     STATUS    RESTARTS   AGE
kube-system   etcd-s2-node1                      1/1       Running   0          33m
kube-system   kube-apiserver-s2-node1            1/1       Running   0          33m
kube-system   kube-controller-manager-s2-node1   1/1       Running   0          35m
kube-system   kube-dns-77bbcbbdfb-lqg2x          3/3       Running   0          14m
kube-system   kube-flannel-ds-fxcz6              1/1       Running   0          5m
kube-system   kube-proxy-pghkr                   1/1       Running   0          34m
kube-system   kube-scheduler-s2-node1            1/1       Running   0          34m
~~~

Now we can join the other two nodes `s2-node{2,3}`:

~~~bash
root@s2-node2:~# kubeadm join --token 708e2c.105019419fd2ae81 10.132.0.3:6443 --discovery-token-ca-cert-hash sha256:dc74321cd0bc970a3bdbce298cc65c1912ea4a3feaaa705dfeeacfab7d01c683

root@s2-node3:~# kubeadm join --token 708e2c.105019419fd2ae81 10.132.0.3:6443 --discovery-token-ca-cert-hash sha256:dc74321cd0bc970a3bdbce298cc65c1912ea4a3feaaa705dfeeacfab7d01c683
[preflight] Running pre-flight checks.
        [WARNING FileExisting-crictl]: crictl not found in system path
[discovery] Trying to connect to API Server "10.132.0.3:6443"
[discovery] Created cluster-info discovery client, requesting info from "https://10.132.0.3:6443"
[discovery] Requesting info from "https://10.132.0.3:6443" again to validate TLS against the pinned public key
[discovery] Cluster info signature and contents are valid and TLS certificate validates against pinned roots, will use API Server "10.132.0.3:6443"
[discovery] Successfully established connection with API Server "10.132.0.3:6443"

This node has joined the cluster:
* Certificate signing request was sent to master and a response
  was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the master to see this node join the cluster.
~~~

Check all nodes are joined:

~~~bash
cephd@s2-node1:~$ kubectl get nodes                 
NAME       STATUS     ROLES     AGE       VERSION
s2-node1   Ready      master    37m       v1.9.2
s2-node2   Ready      <none>    45s       v1.9.2
s2-node3   NotReady   <none>    19s       v1.9.2
~~~

# Ceph and Kubernetes

## Fix ceph bug - not working in latest version

> Adding the rbd binary to the controller node did not work in the latest version.

Kubernetes needs to have the `rbd` binary installed on the controller container.
Therefore we need to build a new docker container:

~~~
FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive
ENV container docker

# Don't start any optional services except for the few we need.
COPY kube-controller-manager /usr/bin
RUN chmod +x /usr/bin/kube-controller-manager
RUN apt-get update && apt-get install -y ceph-common
~~~

And adjust the image on `s2-node1`:

~~~bash
sed -i "s|gcr.io/google_containers/kube-controller-manager-amd64:v1.5.2|docker.io/l4rs/km:v1.9.3|g"
/etc/kubernetes/manifests/kube-controller-manager.yaml
~~~

## RBD provisioner

On the Kubernetes cluster we need to have a pod which runs our rbd provisioner. The provisioner will be the bridge between K8s and ceph.

`provisioner.yml`:

~~~yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
   name: rbd-provisioner
   namespace: kube-system
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: rbd-provisioner
    spec:
      containers:
      - name: rbd-provisioner
        image: "quay.io/external_storage/rbd-provisioner:v0.1.1"
      serviceAccountName: persistent-volume-binder
~~~

~~~bash
kc create -f provisioner.yml
~~~

## Ceph admin key

We need the ceph admin key to use ceph as a backend for kubernetes.
The Ceph key is stored under `/etc/ceph/ceph.client.admin.keyring`:

~~~bash
grep key /etc/ceph/ceph.client.admin.keyring
~~~

For Kubernetes we need it base64 encoded:

~~~bash
root@s2-node1:~# grep key /etc/ceph/ceph.client.admin.keyring | awk '{printf "%s", $NF}'|base64
~~~

## Kubernetes secret

We can store the ceph secret in kubernetes as a yaml file:

~~~yaml
apiVersion: v1
kind: Secret
metadata:
   name: ceph-secret
type: "kubernetes.io/rbd"
data:
   key: [base64 ceph admin key]
~~~

Load the secret into kubernetes:

~~~bash
kc create -f secret.yml
kc create -f secret.yml --namespace=kube-system
~~~

## Ceph Storage class

To use ceph in kubernetes we need to define a ceph storage class:

~~~yaml
apiVersion: storage.k8s.io/v1beta1
kind: StorageClass
metadata:
   name: ceph
provisioner: ceph.com/rbd
parameters:
   monitors: s2-node1,s2-node2,s2-node3
   adminId: admin
   adminSecretName: ceph-secret
   adminSecretNamespace: kube-system
   pool: rbd
   userId: admin
   userSecretName: ceph-secret
~~~

`ceph-secret` is a reference to the ceph secret we created before.

Create the storage Class ceph:

~~~bash
kc create -f storage.yml
~~~

## Ceph persistent volume claim

We can now test if our ceph integration for kubernetes works.
First we create a new file `pvc.yml` which represents our persistent volume claim:

~~~yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-guestbook
  annotations:
    volume.beta.kubernetes.io/storage-class: ceph
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
~~~

we can create the pvc with the following command:

~~~bash
kc create -f pvc.yml
~~~

and check if the pvc is online:

~~~bash
student2@jumphost:~$ kc get pvc
NAME            STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-guestbook   Bound     pvc-3575bb75-1330-11e8-a45d-42010a840003   1Gi        RWO            ceph           31m
~~~

# Sample application

Now our K8s cluster is up and running and we can run an application on top of it.
We are going to deploy a guestbook application.

## Copy the service files

~~~bash
lars@book:~/Downloads$: scp -r guestbook-go/ student2@35.205.224.103:~/guestbook-go
~~~

## Change nodePort to my port

~~~json
{
   "kind":"Service",
   "apiVersion":"v1",
   "metadata":{
      "name":"guestbook",
      "labels":{
         "app":"guestbook"
      }
   },
   "spec":{
      "ports": [
         {
           "port":3000,
           "nodePort":30002,
           "targetPort":"http-server"
         }
      ],
      "selector":{
         "app":"guestbook"
      },
      "type": "NodePort"
   }
}
~~~

## Create application

~~~bash
kc create -f redis-master-controller.json
kc create -f redis-master-service.json
kc create -f redis-slave-controller.json
kc create -f redis-slave-service.json
kc create -f guestbook-controller.json
kc create -f guestbook-service.json
~~~

our pods will be created:

~~~bash
student2@jumphost:~/guestbook-go$ kc get pods
NAME                 READY     STATUS              RESTARTS   AGE
guestbook-4wk28      0/1       ContainerCreating   0          12s
guestbook-kqx2g      0/1       ContainerCreating   0          12s
guestbook-pjzjl      0/1       ContainerCreating   0          12s
redis-master-w8fgm   0/1       ContainerCreating   0          13s
redis-slave-94shb    0/1       ContainerCreating   0          12s
redis-slave-9nhfv    0/1       ContainerCreating   0          12s
~~~

finally all pods should be running:

~~~bash
student2@jumphost:~/guestbook-go$ kc get pods
NAME                 READY     STATUS    RESTARTS   AGE
guestbook-4wk28      1/1       Running   0          41s
guestbook-kqx2g      1/1       Running   0          41s
guestbook-pjzjl      1/1       Running   0          41s
redis-master-w8fgm   1/1       Running   0          42s
redis-slave-94shb    1/1       Running   0          41s
redis-slave-9nhfv    1/1       Running   0          41s
~~~

and we are able to access the application at http://35.205.93.156:30002/:

![Guestbook](guestbook.png)

## Tests

+----------------------------------+-------------------------------+-----------------------------+
| Description                      | Expected result               | Result                      |
+==================================+===============================+=============================+
| Destroy a guestbook pod\         | K8s should loadbalance the\   | K8s creates instantly a new\|
| `kc delete pod guestbook-kvxg8`  | HTTP requests to remaining\   | and loadbalances the\       |
|                                  | pod                           | requests to other pods      |
+----------------------------------+-------------------------------+-----------------------------+
| Shutdown a K8s slave node\       | Enough containers are started\| App waits for DB connection\|
| `ssh cephd@s2-node2 shutdown now`| on the other slave nodes,\    | because redis master    \   |
|                                  | requests are still processed  | is on s2-node2. K8s created\|
|                                  |                               | a new pod on another node,\ |
|                                  |                               | but we lost our data, since |
|                                  |                               | the redis master has no\    |
|                                  |                               | persistent storage          |
+----------------------------------+-------------------------------+-----------------------------+
| Destroy a redis slave pod\       | We still have the second\     | True                        |
| `kc delete pod redis-slave-9zhq4`| redis slave, which should\    |                             |
|                                  | handle the DB connections     |                             |
+----------------------------------+-------------------------------+-----------------------------+
| Destroy all redis slave pods\    | Our data is stored on the\    | K8s instantly creates\      |
| `kc delete pod redis-slave*`     | redis master. After K8s\      | new slave pods. Data is\    |
|                                  | recreated our slave pods,\    | still there.                |
|                                  | we still have our data        |                             |
+----------------------------------+-------------------------------+-----------------------------+
| Destroy the redis master node\   | K8s will recreate the master\ | in the time the master gets\|
| `kc delete pod redis-master-*`   | but we will lose our data,\   | rebuilt, we cannot write to\|
|                                  | since we do not have a\       | the DB. After the rebuild\  |
|                                  | persistent storage.           | we lost our data            |
+----------------------------------+-------------------------------+-----------------------------+

## Ceph Storage for guestbook

Now we want to use ceph as persistent storage. Therefore we use our earlier created [pvc-guestbook](#ceph-persistent-volume-claim).
We need to adjust the `redis-master-controller.json` file:

~~~json
{
   "kind":"ReplicationController",
   "apiVersion":"v1",
   "metadata":{
      "name":"redis-master",
      "labels":{
         "app":"redis",
         "role":"master"
      }
   },
   "spec":{
      "replicas":1,
      "selector":{
         "app":"redis",
         "role":"master"
      },
      "template":{
         "metadata":{
            "labels":{
               "app":"redis",
               "role":"master"
            }
         },
         "spec":{
            "volumes":[
              {
                "name": "guestbook",
                "persistentVolumeClaim": {
                  "claimName": "pvc-guestbook"
                }
              }
            ],
            "containers":[
               {
                  "name":"redis-master",
                  "image":"redis:2.8.23",
                  "ports":[
                     {
                        "name":"redis-server",
                        "containerPort":6379
                     }
                  ],
                  "volumeMounts": [
                    {
                      "mountPath": "/data",
                      "name": "guestbook"
                    }
                  ],
                  "lifecycle": {
                    "postStart": {
                      "exec": {
                        "command": ["/usr/local/bin/redis-cli", "config", "set", "SAVE","10 1"]
                      }
                    }
                  }
               }
            ]
         }
      }
   }
}
~~~

We can now update the K8s configuration and delete the master pod, so the new configuration will become active:

~~~bash
student2@jumphost:~$ kc replace -f guestbook-go/redis-master-controller.json
student2@jumphost:~$ kc delete pod redis-master-z78rm
~~~

The `redis-master` pod will be recreated by k8s:

~~~bash
student2@jumphost:~$ kc get pods
NAME                 READY     STATUS              RESTARTS   AGE
guestbook-ksmwb      1/1       Running             0          57m
guestbook-plvjp      1/1       Running             0          56m
guestbook-wvsdh      1/1       Running             0          57m
redis-master-ms5nr   0/1       ContainerCreating   0          2s
redis-master-z78rm   0/1       Terminating         0          47m
redis-slave-8v98j    1/1       Running             0          51m
redis-slave-nkthc    1/1       Running             0          51m
student2@jumphost:~$ kc get pods
NAME                 READY     STATUS    RESTARTS   AGE
guestbook-ksmwb      1/1       Running   0          57m
guestbook-plvjp      1/1       Running   0          57m
guestbook-wvsdh      1/1       Running   0          58m
redis-master-ms5nr   1/1       Running   0          31s
redis-slave-8v98j    1/1       Running   0          51m
redis-slave-nkthc    1/1       Running   0          51m
~~~

### Test

Our guestbook now looks like:

![redis-before](redis-before.png)

Now we destroy all redis pods (master and slaves):

~~~bash
student2@jumphost:~$ kc delete pod redis-master-ms5nr redis-slave-8v98j redis-slave-nkthc
~~~

K8s destroys all 3 pods, and recreates them:

~~~bash
student2@jumphost:~$ kc get pods
NAME                 READY     STATUS              RESTARTS   AGE
guestbook-ksmwb      1/1       Running             0          1h
guestbook-plvjp      1/1       Running             0          1h
guestbook-wvsdh      1/1       Running             0          1h
redis-master-c678g   1/1       Running             0          6s
redis-slave-88rn2    0/1       ContainerCreating   0          6s
redis-slave-8v98j    1/1       Terminating         0          54m
redis-slave-nkthc    1/1       Terminating         0          54m
redis-slave-pxrhm    1/1       Running             0          6s
~~~

After about 10-15 seconds the new containers should be created:

~~~bash
student2@jumphost:~$ kc get pods
NAME                 READY     STATUS    RESTARTS   AGE
guestbook-ksmwb      1/1       Running   0          1h
guestbook-plvjp      1/1       Running   0          1h
guestbook-wvsdh      1/1       Running   0          1h
redis-master-v9mxh   1/1       Running   0          33s
redis-slave-9w6nr    1/1       Running   0          21s
redis-slave-pxrhm    1/1       Running   0          1m
~~~

On our guestbook website we still have our data:

![redis-after](redis-after.png)

# Thoughts

## What did I learn?

- Setup K8s
- Manipulating the K8s controller node is not a good idea (ceph rbd) / does not work
- I hate JSON! (actually knew that already before.... but once again, I hate it)

## Biggest problems I had

- Ceph pvc for k8s

## Pro / contra K8s

For legacy application  using k8s could be hard. Since they mostly need to run several processes on one box and are not available as docker container.

For application who where build under the microservices concept (with a highly use of docker), using k8s gives some advantages regarding HA, rolling updates and fast deployment. Furthermore developers can use k8s too, which makes the gab between development, testing and production smaller.

# Ceph

## Setup

We are going to create a 4th VM and install docker on it:

~~~bash
gcloud compute instances create s2-node4 --image-family ubuntu-1604-lts --image-project ubuntu-os-cloud --machine-type f1-micro --zone europe-west1-d
gcloud compute ssh s2-node4
~~~

Install needed software

~~~bash
sudo apt-get update && sudo apt-get -y upgrade && sudo apt-get -y dist-upgrade
sudo apt-get install -y docker.io ceph-common radosgw s3cmd
~~~

Pull the ceph demo docker image:

~~~bash
sudo docker pull ceph/demo
~~~

### Start the ceph container

`MON_IP` and `CEPH_PUBLIC_NETWORK` is our node ip address.

~~~bash
sudo docker run -d --net=host -v /etc/ceph:/etc/ceph -e MON_IP=10.132.0.30 -e CEPH_PUBLIC_NETWORK=10.132.0.30/32 ceph/demo
~~~

No we have a working ceph installation:

~~~bash
student2@s2-node4:~$ sudo ceph -w
    cluster a0e8efb7-824c-4f85-9523-450e3f3b31b3
     health HEALTH_OK
     monmap e2: 1 mons at {s2-node4=10.132.0.30:6789/0}
            election epoch 4, quorum 0 s2-node4
      fsmap e4: 1/1/1 up {0=0=up:creating}
        mgr active: s2-node4
     osdmap e10: 1 osds: 1 up, 1 in
            flags sortbitwise,require_jewel_osds,require_kraken_osds
      pgmap v12: 96 pgs, 5 pools, 1681 bytes data, 4 objects
            2471 MB used, 7389 MB / 9861 MB avail
                  88 active+clean
                   8 creating
  client io 4959 B/s wr, 0 op/s rd, 3 op/s wr

2018-02-17 10:18:07.965224 mon.0 [INF] pgmap v10: 88 pgs: 88 creating; 0 bytes data, 0 kB used, 0 kB / 0 kB avail
2018-02-17 10:18:08.972279 mon.0 [INF] mds.? 10.132.0.30:6804/2236064374 up:boot
2018-02-17 10:18:08.977432 mon.0 [INF] fsmap e3: 0/0/1 up, 1 up:standby
2018-02-17 10:18:08.990283 mon.0 [INF] pgmap v11: 88 pgs: 88 active+clean; 1681 bytes data, 2471 MB used, 7389 MB / 9861 MB avail; 2520 B/s wr, 1 op/s
2018-02-17 10:18:08.990849 mon.0 [INF] osdmap e10: 1 osds: 1 up, 1 in
2018-02-17 10:18:08.990938 mon.0 [INF] fsmap e4: 1/1/1 up {0=0=up:creating}
2018-02-17 10:18:08.998564 mon.0 [INF] pgmap v12: 96 pgs: 8 creating, 88 active+clean; 1681 bytes data, 2471 MB used, 7389 MB / 9861 MB avail; 4959 B/s wr, 3 op/s
2018-02-17 10:18:10.005235 mon.0 [INF] osdmap e11: 1 osds: 1 up, 1 in
2018-02-17 10:18:10.009392 mon.0 [INF] mds.0 10.132.0.30:6804/2236064374 up:active
2018-02-17 10:18:10.011500 mon.0 [INF] fsmap e5: 1/1/1 up {0=0=up:active}
2018-02-17 10:18:10.011588 mon.0 [INF] pgmap v13: 96 pgs: 8 creating, 88 active+clean; 1681 bytes data, 2471 MB used, 7389 MB / 9861 MB avail
~~~

## Block storage

We are going to create a ceph block storage and mount it after creating a filesystem.

### Create the block storage volume

~~~bash
sudo rbd create blocktest --size 1G
sudo rbd ls
blocktest
~~~

### Disable unsupported ceph features

~~~bash
sudo rbd feature disable blocktest deep-flatten fast-diff object-map exclusive-lock
~~~

### Mount the volume as block device

This command will mount the ceph volume as a block device:

~~~bash
student2@s2-node4:~$ sudo rbd map blocktest
/dev/rbd0
student2@s2-node4:~$ lsblk
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
rbd0   252:0    0   1G  0 disk
sda      8:0    0  10G  0 disk
└─sda1   8:1    0  10G  0 part /
~~~

### Create a btrfs filesystem on the block device and mount it

~~~bash
student2@s2-node4:~$ mkfs.btrfs /dev/rbd0
btrfs-progs v4.4
See http://btrfs.wiki.kernel.org for more information.

probe of /dev/rbd0 failed, cannot detect existing filesystem.
Use the -f option to force overwrite.
student2@s2-node4:~$ sudo mkfs.btrfs /dev/rbd0
btrfs-progs v4.4
See http://btrfs.wiki.kernel.org for more information.

Detected a SSD, turning off metadata duplication.  Mkfs with -m dup if you want to force metadata duplication.
Performing full device TRIM (1.00GiB) ...
Label:              (null)
UUID:               8d665558-155e-4ef4-9eed-e98c5755da31
Node size:          16384
Sector size:        4096
Filesystem size:    1.00GiB
Block group profiles:
  Data:             single            8.00MiB
  Metadata:         single            8.00MiB
  System:           single            4.00MiB
SSD detected:       yes
Incompat features:  extref, skinny-metadata
Number of devices:  1
Devices:
   ID        SIZE  PATH
    1     1.00GiB  /dev/rbd0
~~~

~~~bash
sudo mkdir /mnt/blocktest
sudo mount /dev/rbd0 /mnt/blocktest
~~~

~~~bash
student2@s2-node4:~$ lsblk
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
rbd0   252:0    0   1G  0 disk /mnt/blocktest
sda      8:0    0  10G  0 disk
└─sda1   8:1    0  10G  0 part /
~~~

~~~bash
root@s2-node4:/mnt/blocktest# echo "hello world" >> /mnt/blocktest/test.txt
root@s2-node4:/mnt/blocktest# ll
total 20
drwxr-xr-x 1 root root   16 Feb 17 10:27 ./
drwxr-xr-x 3 root root 4096 Feb 17 10:26 ../
-rw-r--r-- 1 root root   12 Feb 17 10:27 test.txt
root@s2-node4:/mnt/blocktest# cat test.txt
hello world
~~~

### File storage

Ceph can also be mapped as a network filesystem.
We need to have the ceph admin key for that:

~~~bash
root@s2-node4:/mnt/blocktest# cat /etc/ceph/ceph.client.admin.keyring
[client.admin]
        key = AQBDAYhaj6v5FxAAaZ7VotR7p2BQF3qCTqfwzQ==
        auid = 0
        caps mds = "allow"
        caps mon = "allow *"
        caps osd = "allow *"
~~~

Mount the network volume:

~~~bash
mkdir /mnt/filetest
sudo mount -t ceph 10.132.0.30:/ /mnt/filetest/ -o name=admin,secret=AQBDAYhaj6v5FxAAaZ7VotR7p2BQF3qCTqfwzQ==
~~~

The ceph filesystem will be directly mounted and can be used from now on:

~~~bash
root@s2-node4:/mnt/blocktest# mount -l
10.132.0.30:/ on /mnt/filetest type ceph (rw,relatime,name=admin,secret=<hidden>,acl)
~~~

### Object-Storage

Ceph can also be used as an object storage, S3 and OpenStack cinder API is supported.

#### Create s3 API user

~~~bash
root@s2-node4:/mnt/filetest# sudo radosgw-admin user create --uid=admin --display-name="admin"
{
    "user_id": "admin",
    "display_name": "admin",
    "email": "",
    "suspended": 0,
    "max_buckets": 1000,
    "auid": 0,
    "subusers": [],
    "keys": [
        {
            "user": "admin",
            "access_key": "FSPXRDLU48HDWF3LNSTP",
            "secret_key": "421jjkAFPew0Fq2F4jMC8xnMsoXkQ58XY5LTjsAn"
        }
    ],
    "swift_keys": [],
    "caps": [],
    "op_mask": "read, write, delete",
    "default_placement": "",
    "placement_tags": [],
    "bucket_quota": {
        "enabled": false,
        "max_size_kb": -1,
        "max_objects": -1
    },
    "user_quota": {
        "enabled": false,
        "max_size_kb": -1,
        "max_objects": -1
    },
    "temp_url_keys": []
}
~~~

#### s3cmd

With s3cmd we can browse the object storage, therefore we need to create a configuration file `s3cfg`:

~~~bash
[default]
access_key = FSPXRDLU48HDWF3LNSTP
secret_key = 421jjkAFPew0Fq2F4jMC8xnMsoXkQ58XY5LTjsAn
bucket_location = US
cloudfront_host = cloudfront.amazonaws.com
default_mime_type = binary/octet-stream
delete_removed = False
dry_run = False
enable_multipart = True
encoding = ANSI_X3.4-1968
encrypt = False
follow_symlinks = False
force = False
get_continue = False
gpg_command = /usr/bin/gpg
gpg_decrypt = %(gpg_command)s -d --verbose --no-use-agent --batch --yes --passphrase-fd %(passphrase_fd)s -o %(output_file)s %(input_file)s
gpg_encrypt = %(gpg_command)s -c --verbose --no-use-agent --batch --yes --passphrase-fd %(passphrase_fd)s -o %(output_file)s %(input_file)s
gpg_passphrase =
guess_mime_type = True
host_base = localhost
host_bucket = localhost
human_readable_sizes = False
invalidate_on_cf = False
list_md5 = False
log_target_prefix =
mime_type =
multipart_chunk_size_mb = 15
preserve_attrs = True
progress_meter = True
proxy_host =
proxy_port = 0
recursive = False
recv_chunk = 4096
reduced_redundancy = False
send_chunk = 4096
simpledb_host = sdb.amazonaws.com
skip_existing = False
socket_timeout = 300
urlencoding_mode = normal
use_https = False
verbosity = WARNING
website_endpoint = http://%(bucket)s.s3-website-%(location)s.amazonaws.com/
website_error =
website_index = index.html
~~~

##### Bucket

The s3cmd has problems with the ceph s3 API. I still need to troubelshoot it...
