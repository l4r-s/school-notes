void setup()
{
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  digitalWrite(7, LOW);
  digitalWrite(8, LOW);
}

void loop()
{
  digitalWrite(8, LOW);
  digitalWrite(7, HIGH);
  delay(20);
  digitalWrite(7, LOW);
  digitalWrite(8, HIGH);
  delay(20);
}
