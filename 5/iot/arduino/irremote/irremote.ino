#include <ir_Lego_PF_BitStreamEncoder.h>
#include <boarddefs.h>
#include <IRremote.h>
#include <IRremoteInt.h>

int RECV_PIN = 10;
int led=11;
int i=0;
IRrecv irrecv(RECV_PIN);
decode_results results;

void setup() {
  Serial.begin(9600);
  irrecv.enableIRIn();
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);

}

void loop() {
  if (irrecv.decode(&results)) {
    Serial.println(results.value, DEC);
    if (results.value == 3970 || results.value == 1922){
      digitalWrite(led, HIGH);
    }
    if (results.value == 3969 || results.value == 1921) {
      digitalWrite(led, LOW);
    }
    if (results.value == 1923 || results.value == 3971){
      while (i < 9){
        Serial.println("HIGH");
        digitalWrite(led,HIGH);
        delay(500);
        Serial.println("LOW");
        digitalWrite(led,LOW);
        delay(500);
        i++;
      }
      i = 0;
    } 
    irrecv.resume();
  }

}
