char value;
void setup()
{
  pinMode(9, OUTPUT);
  Serial.begin(9600);
}

void loop()
{
  if (Serial.available()) {
    value = Serial.read();
    digitalWrite(9, value);
  }
}
