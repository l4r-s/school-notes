int eingang=A0;
int sensorwert=0;
int rellay=7;
int led1=10;
int led2=11;
int led3=12;
int high=560;

void setup()
{
  Serial.begin(9600);
  pinMode(rellay, OUTPUT);
  digitalWrite(rellay, HIGH);
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
}
void loop()
{
  sensorwert=analogRead(eingang);
  Serial.print("Sensorwert=");
  Serial.print(sensorwert);
  Serial.print("\n");
  
  if(sensorwert < high) {
    Serial.print("sensorwert < high");
    Serial.print("\n");
    digitalWrite(rellay, LOW);
    delay(3000); 
    digitalWrite(rellay, HIGH);
    if (sensorwert < 520){
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, HIGH);
    } else {
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);
      if (sensorwert < 540) {
        digitalWrite(led1, HIGH);
        digitalWrite(led2, HIGH);
      } else {
        digitalWrite(led1, LOW);
        digitalWrite(led2, LOW);
        digitalWrite(led3, LOW);
        if (sensorwert < 559) {
          digitalWrite(led1, HIGH);
        }
      }
    }
  } else {
    Serial.print("sensorwert > high");
    Serial.print("\n");
    digitalWrite(rellay, HIGH);
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
    digitalWrite(led3, LOW);
  }
  delay(1000);
}
