int value;
void setup()
{
  pinMode(9, OUTPUT);
  Serial.begin(9600);
  digitalWrite(9, LOW);
}

void loop()
{
  if (Serial.available()) {
    value = Serial.read();
    Serial.print("received: \n");
    Serial.print(value);
    Serial.print("\n");
    if ( value == 49) {
      Serial.print("switching HIGH \n");
      digitalWrite(9, HIGH);
    } else if ( value == 48 ) {
      Serial.print("switching LOW \n");
      digitalWrite(9, LOW);
    }
  }
}
