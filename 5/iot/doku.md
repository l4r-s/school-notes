---
title: "Internet of Things"
author: [Lars Bättig (me@l4rs.net)]
date: 2018-04-02
tags: [IoT, Dokumentation]
subtitle: "Dokumentatoin"
titlepage: true
titlepage-rule-height: 4
documentclass: report
lang: de
toc: true
lof: false
lot: false
urlcolor: cyan
...
\newpage

## Arduino

### In Betriebnahme (Linux)
1. Arduino IDE installieren: https://www.arduino.cc/en/Main/Software
2. User der gruppe 'dialout' hinzufügen
3. Arduino IDE starten und unter `Tools` --> `Port` `/dev/ttyUSB0` auswählen

### Blinkendes onboard LED

PIN 13 ist onboard LED (Label `L`) auf dem Arduino.

~~~
void setup()
{
  pinMode(13, OUTPUT); // PIN 13 (Test LED --> L) soll ausgang sein
}

void loop()
{
  digitalWrite(13, HIGH);
  delay(2000); //Warte 2000 Millisekunden (zwei Sekunden)
  digitalWrite(13, LOW);
  delay(1000);
}
~~~

### zwei Blinkende LED

![Zwei Blinkende LEDs](led2.png)

~~~
void setup()
{
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  digitalWrite(7, LOW);
  digitalWrite(8, LOW);
}

void loop()
{
  digitalWrite(8, LOW);
  digitalWrite(7, HIGH);
  delay(200);
  digitalWrite(7, LOW);
  digitalWrite(8, HIGH);
  delay(200);
}
~~~

### LED pulsieren

![LED Pulisieren](pulsieren.png)

~~~
int LED=7;
int helligkeit=0;
int fadeschritte=5;

void setup()
{
   Serial.begin(9600);
  pinMode(LED, OUTPUT);

}
void loop()
{
  Serial.print("Helligkeit \n");
  Serial.print(helligkeit);
  Serial.print ("\n");
  analogWrite(LED, helligkeit);
  helligkeit=helligkeit + fadeschritte;
  delay(1000); //Die LED soll für 25ms (Millisekunden), also nur ganz kurz die

  if(helligkeit==0 || helligkeit== 255){
    fadeschritte= -fadeschritte;
  }
}
~~~


### Photosensor auslessen

![Photosensor](photosensor.png)

~~~
int eingang=A0;
int LED=7;
int sensorwert=0;

void setup()
{
Serial.begin(9600);
  pinMode (LED,OUTPUT);
}
void loop()
{
  sensorwert=analogRead(eingang);
  Serial.print("Sensorwert=");
  Serial.print(sensorwert);
  Serial.print("\n");
  delay(1000);
  if(sensorwert > 512)
  {
    digitalWrite(LED,HIGH);
  }
  else
  {
    digitalWrite(LED,LOW);
  }
    delay(50);
}
~~~

## Pumpensystem Simulieren

Auslessen des NTC MF58 Temperatur sensor.

![Temperatur Sensor](tempmesser.png)

~~~
int eingang=A0;
int sensorwert=0;

void setup()
{
Serial.begin(9600);
}
void loop()
{
  sensorwert=analogRead(eingang);
  Serial.print("Sensorwert=");
  Serial.print(sensorwert);
  Serial.print("\n");
  delay(1000);
}
~~~

### Temperaturen und Werte (ungenau):

| Temperatur            | Wert |
|-----------------------|------|
| Raumtemperatur ~20C   | 500  |
| Finger ~30C           | ~560 |
| 0C                    | 300  |
| 50C                   | 800  |


### Pumpensteuerung

Fält der Wert unter 30C soll die Pumpe (simuliert mit LED) für 3 Sekunden eingeschaltet werden.
Die nächste Messung soll nach 30C erfolgen.

~~~
int eingang=A0;
int sensorwert=0;
int rellay=7;
int high=560;

void setup()
{
  Serial.begin(9600);
  pinMode(rellay, OUTPUT);
  digitalWrite(rellay, HIGH);
}
void loop()
{
  sensorwert=analogRead(eingang);
  Serial.print("Sensorwert=");
  Serial.print(sensorwert);
  Serial.print("\n");

  if(sensorwert < high) {
    Serial.print("sensorwert < high");
    Serial.print("\n");
    digitalWrite(rellay, LOW);
    delay(3000);
    digitalWrite(rellay, HIGH);
  } else {
    Serial.print("sensorwert > high");
    Serial.print("\n");
    digitalWrite(rellay, HIGH);
  }
  delay(30000);
}
~~~

![Pumpensteuerung Simulation](pumpe.jpg)

### Ventilsteuerung

Nun soll ein Ventil gesteuert werden. Dieses wird mit 3 LEDs simuliert.

| Wert      | aktive LEDs      |
|-----------|------------------|
| 500 - 520 | led1, led2, led3 |
| 521 - 540 | led1, led2       |
| 541 - 559 | led1             |
| > 560     | -                |

~~~
int eingang=A0;
int sensorwert=0;
int rellay=7;
int led1=10;
int led2=11;
int led3=12;
int high=560;

void setup()
{
  Serial.begin(9600);
  pinMode(rellay, OUTPUT);
  digitalWrite(rellay, HIGH);
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
}
void loop()
{
  sensorwert=analogRead(eingang);
  Serial.print("Sensorwert=");
  Serial.print(sensorwert);
  Serial.print("\n");

  if(sensorwert < high) {
    Serial.print("sensorwert < high");
    Serial.print("\n");
    digitalWrite(rellay, LOW);
    delay(3000);
    digitalWrite(rellay, HIGH);
    if (sensorwert < 520){
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, HIGH);
    } else {
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);
      if (sensorwert < 540) {
        digitalWrite(led1, HIGH);
        digitalWrite(led2, HIGH);
      } else {
        digitalWrite(led1, LOW);
        digitalWrite(led2, LOW);
        digitalWrite(led3, LOW);
        if (sensorwert < 559) {
          digitalWrite(led1, HIGH);
        }
      }
    }
  } else {
    Serial.print("sensorwert > high");
    Serial.print("\n");
    digitalWrite(rellay, HIGH);
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
    digitalWrite(led3, LOW);
  }
  delay(30000);
}
~~~

![Ventil Simulation](ventil.jpg)

## Vernetzte IoT Systeme

### Master / Slave System

Auf dem Master (UNO) ist ein Schalter (representiert durch Kabelstecken) welcher via Serial dem Slave (Nano) mitteilt er soll das LED einschalten wenn Schalter auf 1 bzw. Ausschalten wenn Schalter auf 0.

![Master / Slave System](schalter_master_slave.jpg)

#### Master code

~~~
int pinValue;

void setup()
{
  Serial.begin(9600);
  pinMode(4, INPUT);
}

void loop()
{
  pinValue = digitalRead(4);
  if (pinValue == 1){
    Serial.print(true);
  } else if (pinValue == 0){
    Serial.print(false);
  }


}
~~~

#### Slave Code

~~~
int value;
void setup()
{
  pinMode(9, OUTPUT);
  Serial.begin(9600);
  digitalWrite(9, LOW);
}

void loop()
{
  if (Serial.available()) {
    value = Serial.read();
    Serial.print("received: \n");
    Serial.print(value);
    Serial.print("\n");
    if ( value == 49) {
      Serial.print("switching HIGH \n");
      digitalWrite(9, HIGH);
    } else if ( value == 48 ) {
      Serial.print("switching LOW \n");
      digitalWrite(9, LOW);
    }
  }
}
~~~

## IR Receiver

Taste 1: Schaltet LED ab
Taste 2: Schaltet LED an
Taste 3: Schaltet LED in blinking mode

~~~
#include <ir_Lego_PF_BitStreamEncoder.h>
#include <boarddefs.h>
#include <IRremote.h>
#include <IRremoteInt.h>

int RECV_PIN = 10;
int led=11;
int i=0;
IRrecv irrecv(RECV_PIN);
decode_results results;

void setup() {
  Serial.begin(9600);
  irrecv.enableIRIn();
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);

}

void loop() {
  if (irrecv.decode(&results)) {
    Serial.println(results.value, DEC);
    if (results.value == 3970 || results.value == 1922){
      digitalWrite(led, HIGH);
    }
    if (results.value == 3969 || results.value == 1921) {
      digitalWrite(led, LOW);
    }
    if (results.value == 1923 || results.value == 3971){
      while (i < 9){
        Serial.println("HIGH");
        digitalWrite(led,HIGH);
        delay(500);
        Serial.println("LOW");
        digitalWrite(led,LOW);
        delay(500);
        i++;
      }
      i = 0;
    }
    irrecv.resume();
  }

}
~~~

### Weiterleiten der Empfangenen IR Daten zu Telegram Bot API

Der Arduino UNO gibt alle Empfangenen IR daten via Serial weiter, mit folgendem Bash script können diese an die Telegram Bot API weitergeleitet werden:

~~~bash
#!/bin/bash
while read -r line < /dev/ttyUSB1; do
  echo $line
  curl -s -X POST https://api.telegram.org/bot$TOKEN/sendMessage -d chat_id=$CHATID -d text="$line"
done
~~~

![IR - Telegram Bot](ir.jpg)

## IR Sender

Für den Empfänger verwende ich den gleichen code wie bei IRReceive.
Für den Sender wurde folgender Code verwendet:

~~~
#include <IRremote.h>
IRsend irsend;
void setup() {
Serial.begin(9600);
}
void loop()
{
  irsend.sendSony(3969, 12);
  delay(8000);
  irsend.sendSony(3970, 12);
  delay(8000);
  irsend.sendSony(1923, 12);
  delay(8000);
}
~~~

![IR Sender](irsend.jpg)
