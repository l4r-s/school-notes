#!/bin/bash
while read -r line < /dev/ttyUSB1; do
  echo $line
  curl -s -X POST https://api.telegram.org/bot$TOKEN/sendMessage -d chat_id=$CHATID -d text="$line"
done
