# Einführung

Unterricht: [https://smlz.github.io/tsbe-2018fs/web/](https://smlz.github.io/tsbe-2018fs/web/)


# UX Design

- UX ist nicht nur Digital, kann auch unboxing sein z.B.
- User ist am wichtigsten, alles wird für den Menschen (Benutzer) gemacht

## Prozess

1. Research (was will der Kunde eigentlich)
2. User Interview (zuhören, Feedback einholen)
3. Skizzieren (am besten mit Dev und Business, ziel: Idee visualisiern und vom gleichen reden)
4. Prototyp (besten Ideen und Elemente zusammenbringen --> dies auf das Testgerät bringen; möglichst viel Faken mit wenig aufwand, schnell ist key)
5. User Tests (User Feedback einholen, klare Aufgabe geben weil geht ja noch nicht alles. Szenario ausdenken. User soll nicht getestet werden, sondern die Idee. Kritische / schlechte dinge als feedback sind am wertvollsten)
--> loop


# Research SSH mgmt app:

Top 3
https://www.youtube.com/watch?v=CwXou5kUYJo Electrum App für ssh mgmt, zugriff via console
https://github.com/emre/storm cli und web ui
https://github.com/mscdex/ssh2




https://chrome.google.com/webstore/detail/secure-shell-app/pnhechapfaindjhompbnflcldabbghjo?hl=en
Mgmt: https://ska.xiven.com/servers/production01.example.com
deployed keys auf server.....
(testuser/testuser)
https://github.com/billchurch/WebSSH2 ssh in browser
https://lifehacker.com/5783594/firessh-is-a-browser-based-ssh-client-written-entirely-in-javascript
js ssh client/server https://github.com/mscdex/ssh2 (Start an interactive shell session)
lightweight ssh client: https://github.com/VanCoding/NodeSSH
