# 10 Schritte zur erfolgreichen Diplomarbeit

1. Projekt gut abgrenzen ( Was mach ich, was nicht)
  a) experte eins kennt evt. Porjekt, 2. expert eher nicht
  b) beide Experten müssen gleiches verständins haben --> missverständnisse vermeiden
2. Projektziele klar und messbar beschreiben
3. Ausgangslage fir "Nicht-Insider" beschreiben
4. Messbarkeit der Lösungsanforderungen sicherstellen
5. "Echte" Lösungsvarianten nachvollziehbar beurteilen
6. Umsetzung nachvollziebhar dokumentieren
7. Absprechen, Entscheide nachvollziehbar protokollieren
  a) Ordnerstruktur, von anfang an alles saueber ablegen
8. Genügend Zeit für Dokumentation einplanen
  a) lieber ein-zwei feature weniger, dafür gute Dokumentation
  b) zeit für review und drucken
9. Termine für Sitzungen rechtzeitig definieren
10. Richtlinen der Prüfungskommission nicht vergessen
  a) hab ich an alles gedacht, von Anfang an checken
  b) Grundstruktur des Dokumentes von anfang an

# Ablauf Diplomarbeit Dokument
1. Diplomeingabe
2. Initilisierung
  a) Projektinitialisierung
  b) Projektauftrag / Projektplan / Ergebnisse Studie --> Presenation Zwischenmeeting

Diplombericht: Begrenzte Sicht mit dingen von Projektmgmt, erzählen was gemacht wurde. (z.B. Konzepte wurden erarbeitet, referenz in Diplombericht.) Diplombericht ist roter Faden des Projektes.
Diplombericht von Initialisertung bis Abshluss dokumentiert. Machbarkeitsstudie: Realisierung und einführung beschreiben im Diplombericht.


Kickoff: Projektplan / Projektinitialisierung

# Kickoff Meeting
- Projektinitialisierung Auftrag erarbeiten
- Porjektplan
- Kickoff Presentation
- Ausgangslage Aufziegen
- Erklären der Projektziele, Projektorganisation, Projektablauf, Abgrenzungen
- Freigabe Projektinitialisierungauftrag durch die Experten (Dokumentieren)
  --> Feedback von Experten ist wichtig weil Kickoff noch nicht bewertet

# Zwischenmeeting
- Inhalt Initialisierung (Ausgangslage, Anforderungen, Varianten (Studie))
- Plan für Varianten Umsetzung
- Kriterien an die Lösungen
- Analyse Ist situation
- Variantenentscheid

# Abschluss Meeting
- Konzeption und realisierung der LöLösungsvarianten
- übergabe Diplombericht an Experten
- Aufzeigen dese wegs wie die Lösung realisiert worden Ist
- Präsentation der Lösung
- Aufzeigen Projekterkentnisse (Erreichung, Projektziele, Projektcontrolloing, eigene Reflektion)
- Bewertung Berichterstattung und Presentation
-

# Vorgehen bei Dokumentation
- Dokument mit Grundstruktur sicherstellen
  - Inhalt überschrift bis 2. Stuffe definieren
- Quellen notieren
- Pro kapitel im voraus mit Stichworten den geplanten Inhalt definieren
- Arbeitsjournal jeden Arbeitstag erledigen
- NAch phasenende den Text ausschreiben
  - Text paar Tage liegen lassen, nochmals
  - Regelmässig Text blöcke abschliessen
- review

## Abschluss
- Verweise, Nummerierung nochmals Prüfen
- Text an nicht beteiligte Personen (technisch) geben
- Druck sauber binden lassen
  a) min eine Woche einplanen
- Dokument rechtzeitig an Empfänger
  a) Unterschrift

# Management Summary
- Paar Tage bauchweh. Keine Schreibfehler zulassen!
- Zusammenfassung der Arbeit auf A4
- Fakten, keine Phasenaufzählung
- Wir glauben... wir sind der Meinung gehört nicht in ein Mgmt Summary

# Arten von Zielen
- Nutzen in den Vordergund / nicht technisch ist geil...
- Kostenvergleich (Wartung miteinbeziehen)
- Nutzwertanalyse (Kriterien müssen sehr klar sein) --> nachvollziehbarkeit

# Konzept
- überlegen was für Konzepte braucht es

# Logbuch
- von anfang an, nicht erst am Schluss
- Mindestens Wochenweise
- Was für Probleme
- Was habe ich gemacht
- Was habe ich falsch gemacht


# Q/A
Q: Können HERMES "dinge" Ausgelassen werden?
A: Projektauftrag wird am "meisten" bewertet, Bewertungsraster: Diplombericht, Projektauftrag, Projektplan. Richtlinen (von Alex): Grau hinterlegte dinge werden bewertet. Es braucht nicht jedes Hermes Dokument, im Bewertungsraster nachschauen!

Q: Wie Bericht auf 35 Seiten "kürzen"?
A: Dokument gibt gutes Raster vor. Detailierte Dokumente kommen in den anhang, nach da verweisen

Q: Projektinitialisierung freigeben von Experten. soll dies am Kickoff freigegeben werden
A: meist gibt es noch Feedback, nach Meeting feedback einfliessen lassen. Freiagbe per mail reicht

Q: Zwischen Meeting auch nach Konzept möglich
A: theoretisch ja, aber besser nicht. weil einfluss von Feedback wichtig

Q: Word Vorlage für Titelbild, wo ist dies?
A: Kommende Woche, im 5. Semester

Q: Präsentation für Refresh PRM?
A: kommt via Mail von Ilias, inkl. noch Text und allen relevanten Dokumenten (aus sicht Marc Aeby)

Q: Wir sollten auf Ressourcen von vorherigen Semest ern zugreifen können, geht aber nicht mehr...
A: Joa... is halt kacke... / Wer was braucht von Marc, melden

Q: Bewertungsbogen, auf Seite 1 wird auf Seite 4 referenziert. Gibt aber keine Seite 4...
A: Referenz zeigt auf Seite 2, bzw. was sind mit den Pünkten gemeint.

Q: wann werden Expterne informiert?
A: ging gestern raus

Q: Diplomeingabe und Projektinitialisierung sind ähnlich oder?
A: JA, aber weichen z.T. von einander ab. z.B. Projektziele sind bei der eingabe häufig nicht so detailiert

Q: Projektauftrag, was ist zu beachten wenn nicht wirklich ein Auftraggebber besteht?
A: Expert kann als Auftragebber deklariert werden, dieser muss dies auch abnehmen.

Q: Wenn die "alte" Phase voranalyse drin ist, sollte die raus?
A: Ja, ausser die firma fordert dies

Q: Wie genau muss der Terminplan sein?
A: Gesunder Menschenverstand (sollte auf A3 platz haben). Haupttätigkeiten und Arbeitspakete drauf

Q: Wenn z.B. Wochenwies geplannt, kann das Logbuch dann auch Wochenweise sein.
A: JA

Q: Wenn hilfe verlangt wird, muss das ja deklariert werden. Kann ein auslagerungsauftrag erstellt werden?
A: JA, gesunder Menschenverstand. Muss aber sauber deklariert sein.

Q: Logbuch ist nicht teil des Berichtes?
A: Ja, Logbuch (salopp) ist Tagebuch des Diplomberichtes

Q: Lessions Learnde im Logbuhc?
A: Jup.

Q: Wie Einführung für Prototype?
A: Aufzeigen was gemacht werden muss um in die Produktion zu gelangen.

Q: Legal dinge für Geheimhaltungen?
A: Geheimhaltungen können vereinbart werden. Aber nicht übertreiben. / Experten haben schweigepflicht, die Schule darf Diplomarbeit nicht einfach so weiterverwendet werden. Schule gibt keine Diplomarbeiten raus.
