# ISE-1 SDP

## Generell
- Teste anschauen (sehr fest angelehnt an die Semestertests)
- evt. auch solche fragen die nicht in den Tests waren
- kein Spicker

## Linux
- /etc/network/interfaces
- /etc/resolv.conf
- dns-search --> nslookup jupiter --> möglicher fehler: dns search string nicht gestetzt bzw. zone nicht angegeben
- dns-nameservers
- scp
- syslog /var/log/syslog
-  
## Proxy
- Einsatzgebiet, wenn und warum (nicht detailiert)
- einmal durchlesen sollte reichen (sagt Megert)

## Mail
- Theoretische fragen ganz sicher
- Config eher nicht
	- evt. was für dienste es gibt
- IMAP / POP

## Apache
- https nicht dabei
- dateiname --> {url}.conf (best practices)
- VirtualHost
- ServerName nicht verhandelbar (www.zone)
- DocumentRoot muss passen
- basic auth (user, gruppen, keyfile chabis)
- a2ensite


## Samba
- kommt nicht

## DNS (bind)
- Theorie
- DDNS Konfig
- key file wird von bind erstellt für ddns
- key file: berechtigung für diese (chmod, chown) müssen wir nicht wissen
- include key file
- config anpassungen gemäss vorgabe
- forward reverse zone, syntax
- ddns zone: pfad in /var/cache weil best practices ohne ddns pfad: /etc/bind
- named.conf.local
- forward zone file (SOA nur primary NS, serial erhöhen, sonst nix mit refresh und so)
- reverse zone file
- NS recorder: alle nameserver
- grafik muss auf zonen files übertragen werden
- A record des Nameservers richtig setzen !!!!!!
- allow query
- forwarders

## DHCP
- Theorie
- DHCP konfig (wäg ddns)
- dhcp config
	- keine direktive die rein muss, können aber falsch sein (wie z.B. host mit hardware ethernet xx:xx:xx:xx:xx fixed address)
	- korrigieren von bestehender config
	- ddns-domainname
	- include key file für ddns
	- subnet config syntax (netzadresse, range, netmask)
	- ddns zonen (syntax, key file, reverse, forward)
	- key file: key name ist nicht gleich key file pfad

