# SDP Java 1

## if
- entscheidungen die getroffen werden

~~~java
if ( bolean wert - true oder false als rückgabe ) {
   .... code .....
}
else {
   .... code ....
}
~~~

## while / for
- wiederholung

~~~java
while ( bolean wert - true ) {
   .... code ....
}
~~~

~~~java
for (int i=0; i<10; i++) {
   .... code ....
}
~~~

