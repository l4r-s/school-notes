# Table of content

## Test dates 4th semester
- Datenbank
  - 22.09.17
  - ???
  - ???
- Englisch
  - ???
  - ???
  - ???
- Internet Services
  - 27.10.17
  - 08.12.17
  - 19.01.18
- IT-Sicherheit
  - 22.09.17
  - 24.11.17
  - 12.01.18
- Java
  - 22.09.17
  - 02.12.17
  - 20.01.17
- Leadership
  - ???
  - ???
  - ???
- Physik
  - 15.09.17
  - 17.11.17
  - 12.01.18
- Security Incident Managagement
  - ???
  - ???
  - ???
- Service Management
  - 22.09.17
  - 15.12.17
  - ???

## 4th semester
  - [Datenbank](4/Datenbank)
  - [Englisch](4/Englisch)
  - [Internet Services](4/InternetServices)
  - [IT-Sicherheit](4/IT-Sicherheit)
  - [Java](4/Java)
  - [Leadership](4/Leadership)
  - [Physik](4/Physik)
  - [Security Incident Management](4/SecurityIncidentManagement)
  - [Service Management](4/ServiceManagement)

## 3th semester
 - [Betriebsprozesse](3/Betriebsprozesse)
 - [Betriebssysteme](3/Betriebssysteme)
 - [ELODIG](3/ELODIG)
 - [English](3/English)
 - [Kommunikation](3/Kommunikation)
 - [LAN](3/LAN)
 - [LAN-Praktisch](3/LAN-Praktisch)
 - [Physik](3/Physik)
 - [Projektmanagement](3/Projektmanagement)
 - [Raspberry](3/Raspberry)
 - [Portfolio](3/Raspberry/Portfolio/Portfolio.md)

# Contribute
If you would like to contribute to this repository please make a pull request or ask for contribute permission.
